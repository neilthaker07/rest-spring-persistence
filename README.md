# CMPE 275 - LAB 2 #
* Afreen Patel - 011811397  
* Neil Thaker - 011538215  
* Purvesh Kothari -  

### What is this repository for? ###

* Spring Boot Project, Maven
* MySQL database
* REST APIS, CRUD operations
* JPA
* Spring Tool Suite IDE

### How do I get set up? ###

* Open Spring Tool Suite IDE.
* Import project using maven.
* Then Run Main file as a spring boot application.
* Open MySQL Database and create database with name : jba (it will be changed afterwards)
* Open postman. Hit URL on 8080 with proper link.

### Contribution guidelines ###


### Who do I talk to? ###

